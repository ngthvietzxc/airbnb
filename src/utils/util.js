const random = require("lodash/random");

const randomF = (min, max, num) => random(min, max, true).toFixed(num);
const randomI = (min, max) => random(min, max);

// eslint-disable-next-line import/no-anonymous-default-export
export default { randomF, randomI };
