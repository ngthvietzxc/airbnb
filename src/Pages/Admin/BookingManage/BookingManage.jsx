import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  deleteBooking,
  getBooking,
} from "../../../redux/action/bookingManageAction";
import moment from "moment";
import { NavLink } from "react-router-dom";
import { Pagination } from "@mui/material";
import { DatePicker, Button } from "antd";
import AddBooking from "./AddBooking";
import { FiTrash2 } from "react-icons/fi";

export default function BookingManage() {
  const dispatch = useDispatch();

  const bookings = useSelector(
    (state) => state.bookingManageReducer.bookingData
  );

  useEffect(() => {
    dispatch(getBooking());
  }, [dispatch]);

  const [currentPage, setCurrentPage] = useState(1);
  const itemsPerPage = 7;
  const totalPages = Math.ceil(bookings.length / itemsPerPage);
  const handlePageChange = (event, page) => {
    setCurrentPage(page);
  };

  const [filterDate, setFilterDate] = useState(null);
  const handleFilterChange = (date) => {
    setFilterDate(date);
  };

  useEffect(() => {
    dispatch(getBooking(filterDate));
  }, [dispatch, filterDate]);

  const startIndex = (currentPage - 1) * itemsPerPage;
  const endIndex = startIndex + itemsPerPage;
  const currentPageRooms = [...bookings].reverse().slice(startIndex, endIndex);
  // Add Booking
  const handleAddBookingClick = () => {
    setShowAddBooking(!showAddBooking);
  };
  //Xóa User
  const handleDeleteBooking = (id) => {
    dispatch(deleteBooking(id));
  };

  const [showAddBooking, setShowAddBooking] = useState(false);
  return (
    <div className="container mx-auto pt-5">
      <h1 className="pl-5 pb-5 text-3xl font-bold ">
        Quản lý thông tin đặt phòng
      </h1>
      <div className="px-10 py-5 m-5 border-solid border-2 border-black bg-gray-300">
        <div className="flex pb-5 justify-between">
          <div className="flex items-center">
            <label className="mr-2 font-semibold">Chọn ngày đến:</label>
            <DatePicker onChange={handleFilterChange} />
          </div>
          <div className="w-48 ">
            <Button
              type="primary"
              htmlType="submit"
              className="   hover:bg-blue-800 text-white font-semibold rounded px-4 py-2 w-full flex justify-center items-center"
              style={{ backgroundColor: "#35425e" }}
              onClick={handleAddBookingClick}>
              <span className="mx-auto">Thêm đặt phòng</span>
            </Button>
          </div>
        </div>
        <p className="text-center pb-5 text-gray-800 text-xl font-bold">
          DANH SÁCH THÔNG TIN ĐẶT PHÒNG
        </p>
        <table className="min-w-full border border-gray-300 drop-shadoww">
          <thead className="bg-gray-500 text-white">
            <tr>
              <th className="py-2 px-3 border-r w-16 font-semibold">ID</th>
              <th className="py-2 px-3 border-r w-36 font-semibold">
                Mã Phòng
              </th>
              <th className="py-2 px-3 border-r w-36 font-semibold">
                Ngày Đến
              </th>
              <th className="py-2 px-3 border-r w-36 font-semibold">Ngày Đi</th>
              <th className="py-2 px-3 border-r w-36 font-semibold">
                Số Lượng Khách
              </th>
              <th className="py-2 px-3 border-r w-36 font-semibold">
                Mã Người Dùng
              </th>
              <th className="py-2 px-3 border-r w-64 font-semibold">
                Tên Người Dùng
              </th>
              <th className="py-2 px-3 font-semibold">Actions</th>
            </tr>
          </thead>
          <tbody>
            {currentPageRooms
              .filter((booking) => {
                if (!filterDate) {
                  return true;
                }
                const arrivalDate = moment(
                  booking.ngayDen,
                  "YYYY-MM-DDTHH:mm:ss"
                ).format("YYYY-MM-DD");
                return arrivalDate === filterDate.format("YYYY-MM-DD");
              })
              .map((booking) => (
                <tr key={booking.id} className="border-b">
                  <td className="py-2 bg-white  px-4 border-r">{booking.id}</td>
                  <td className="py-2 bg-white  px-10 border-r">
                    {booking.maPhong}
                  </td>
                  <td className="py-2 bg-white  px-10 border-r">
                    {moment(booking.ngayDen).format("DD/MM/YYYY")}
                  </td>
                  <td className="py-2 bg-white  px-10 border-r">
                    {booking.ngayDi
                      ? moment(booking.ngayDi, "YYYY/MM/DD").format(
                          "DD/MM/YYYY"
                        )
                      : "-"}
                  </td>
                  <td className="py-2 bg-white  px-10 border-r">
                    {booking.soLuongKhach}
                  </td>
                  <td className="py-2 bg-white  px-10 border-r">
                    {booking.maNguoiDung}
                  </td>
                  <td className="py-2 bg-white  px-10 border-r">Name</td>
                  <td className="py-2 bg-white  px-10 space-x-5 flex">
                    <div className="w-24 h-8">
                      <NavLink
                        to={`/admin/bookings/${booking.id}`}
                        type="warning"
                        className=" text-white font-semibold text-xs rounded   w-full h-full flex justify-center items-center"
                        style={{ backgroundColor: "#35425e" }}>
                        <span className="mx-auto">Chi tiết</span>
                      </NavLink>
                    </div>
                    <div className="w-10 h-8">
                      <button
                        className="bg-red-600 text-white text-lg justify-center flex rounded h-full w-full flex  items-center"
                        onClick={() => handleDeleteBooking(booking.id)}>
                        <FiTrash2 />
                      </button>
                    </div>
                  </td>
                </tr>
              ))}
          </tbody>
        </table>
        <div className="flex justify-center pt-7 pb-3">
          <Pagination
            variant="outlined"
            color="primary"
            count={totalPages}
            page={currentPage}
            onChange={handlePageChange}
          />
        </div>
      </div>
      {showAddBooking && (
        <div className="fixed inset-0 flex items-center justify-center z-50 bg-black bg-opacity-50">
          <div className="bg-white p-8 rounded-lg">
            <AddBooking onClose={handleAddBookingClick} />
          </div>
        </div>
      )}
    </div>
  );
}
