import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Divider, Form, Input } from "antd";
import { useParams } from "react-router-dom";

import {
  getLocationById,
  putLocationById,
} from "../../../redux/action/locationManageAction";

export default function EditLocation() {
  const { id } = useParams();
  const dispatch = useDispatch();

  const location = useSelector(
    (state) => state.locationManageReducer.locationDataById
  );

  const [tenViTri, setTenViTri] = useState("");
  const [tinhThanh, setTinhThanh] = useState("");
  const [quocGia, setQuocGia] = useState("");
  const [hinhAnh, setHinhAnh] = useState(null);

  useEffect(() => {
    dispatch(getLocationById(parseInt(id)));
  }, [dispatch, id]);

  useEffect(() => {
    if (location) {
      setTenViTri(location.tenViTri);
      setTinhThanh(location.tinhThanh);
      setQuocGia(location.quocGia);
      setHinhAnh(location.hinhAnh);
    }
  }, [location]);

  const handleSaveLocation = () => {
    const updatedLocationData = {
      tenViTri,
      tinhThanh,
      quocGia,
      hinhAnh,
    };
    dispatch(putLocationById(parseInt(id), updatedLocationData));
    setEditingMode(false);
  };

  const [editingMode, setEditingMode] = useState(false);

  const renderEdit = () => (
    <div className="fixed inset-0 flex items-center justify-center bg-opacity-75 bg-gray-500">
      <div className="bg-white p-8 rounded" style={{ width: 500 }}>
        <div className="flex w-full justify-center">
          <h2 className="font-bold text-xl pb-5">Thay đổi thông tin</h2>
        </div>

        <Form name="basic" labelCol={{ span: 4 }} onFinish={handleSaveLocation}>
          <Form.Item label="Tên vị trí">
            <Input
              value={tenViTri}
              onChange={(e) => setTenViTri(e.target.value)}
            />
          </Form.Item>

          <Form.Item label="Tỉnh thành">
            <Input
              value={tinhThanh}
              onChange={(e) => setTinhThanh(e.target.value)}
            />
          </Form.Item>

          <Form.Item label="Quốc gia">
            <Input
              value={quocGia}
              onChange={(e) => setQuocGia(e.target.value)}
            />
          </Form.Item>

          <Form.Item label="Hình ảnh">
            <Input
              value={hinhAnh}
              onChange={(e) => setHinhAnh(e.target.value)}
            />
          </Form.Item>

          <Form.Item className="flex justify-end">
            <div className="flex space-x-2">
              <div>
                <button
                  type="primary"
                  htmlType="submit"
                  className="  hover:bg-blue-800 text-white font-semibold rounded px-4 py-2 w-full flex justify-center items-center"
                  style={{ backgroundColor: "#35425e" }}>
                  <span className="mx-auto">Lưu</span>
                </button>
              </div>
              <div>
                <button
                  type="primary"
                  className="  hover:bg-blue-800 text-white font-semibold rounded px-4 py-2 w-full flex justify-center items-center"
                  style={{ backgroundColor: "#35425e" }}
                  onClick={() => setEditingMode(false)}>
                  Hủy
                </button>
              </div>
            </div>
          </Form.Item>
        </Form>
      </div>
    </div>
  );

  return (
    <div className="container mx-auto pt-5">
      <h1 className="pl-5 pb-5 text-3xl font-bold">
        Thông tin chi tiết vị trí (ID: {location.id})
      </h1>
      <div className="px-10 py-5 m-5 border-solid border-2 border-black bg-gray-300">
        <div className="flex">
          <div className="w-1/2">
            <h2 className="pb-5 text-2xl font-bold pb-10">
              {location.tenViTri}
            </h2>

            <p>
              <span className="font-semibold">Tên vị trí : </span>
              {location.tenViTri}
            </p>
            <Divider />
            <p>
              <span className="font-semibold">Tỉnh thành : </span>
              {location.tinhThanh}
            </p>
            <Divider />
            <p>
              <span className="font-semibold">Quốc gia : </span>
              {location.quocGia}
            </p>
            <Divider />
            <p>
              <span className="font-semibold">Mô tả : </span> Lorem ipsum dolor
              sit amet consectetur adipisicing elit. Nisi, fugiat a! Possimus
              sequi voluptas iste modi sunt doloremque libero, repudiandae rerum
              ut magni debitis dolor amet eos ea maxime. Beatae. Lorem ipsum
              dolor sit amet consectetur adipisicing elit. Beatae. Lorem ipsum
              dolor sit amet consectetur adipisicing elit.
            </p>
            <Divider />
            <div>
              {!editingMode && (
                <Form.Item wrapperCol={{ span: 4 }}>
                  <button
                    type="primary"
                    className="  hover:bg-blue-800 text-white font-semibold rounded px-4 py-2 w-full flex justify-center items-center"
                    style={{ backgroundColor: "#35425e" }}
                    onClick={() => setEditingMode(true)}>
                    <span className="mx-auto">Chỉnh sửa</span>
                  </button>
                </Form.Item>
              )}
              {editingMode && renderEdit()}
            </div>
          </div>
          <div className="w-1/2">
            <div className="p-10 w-auto h-auto">
              <img src={location.hinhAnh} alt="" />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
