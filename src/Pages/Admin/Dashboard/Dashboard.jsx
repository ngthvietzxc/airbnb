import React from "react";

export default function Dashboard() {
  return (
    <div
      className="bg-cover bg-center h-screen"
      style={{
        backgroundImage: "url(https://wallpapercave.com/wp/wp10784413.jpg)",
      }}>
      <p className=" text-4xl flex font-bold  justify-center h-full animate-bounce pt-48 ">
        ADMIN SYSTEM
      </p>
    </div>
  );
}
