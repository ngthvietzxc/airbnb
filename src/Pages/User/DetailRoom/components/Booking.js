import React, { useEffect, useState } from "react";
import { DateRangePicker } from "react-date-range";
import { formatDate, toDate, sortDate, fillDates } from "../../../../lib/date";
import { differenceInDays } from "date-fns";
import { useDispatch, useSelector } from "react-redux";
import moment from "moment";
import { message } from "antd";
import { getOrderRoom } from "../../../../Services/orderServ";
import { setUserOrderRoomAction } from "../../../../redux/action/userOrderRoomAction";

function Booking({ roomDetail }) {
  const dispatch = useDispatch();

  const userSingIn = useSelector((state) => state.userLoginReducer.userSingIn);
  const userLoginInfo = userSingIn?.user;

  const maNguoiDung = userLoginInfo?.id;
  console.log("maNguoiDung: ", maNguoiDung);

  const [guest, setGuest] = useState(1);

  const increaseGuest = () => {
    setGuest((prevGuest) => {
      let newGuest = prevGuest + 1;
      return newGuest;
    });
  };

  const decreaseGuest = () => {
    setGuest((prevGuest) => {
      let newGuest = prevGuest - 1;
      if (newGuest < 1) {
        newGuest = 1;
      }
      return newGuest;
    });
  };

  const handleBooking = () => {
    !userLoginInfo && message.warning("Cần phải đăng nhập để đặt phòng");
    userLoginInfo &&
      dispatch(setUserOrderRoomAction(orderRoomByUserInfo, onSuccess, onError));
  };

  let onSuccess = () => {
    message.success("Đặt phòng thành công");
  };
  let onError = () => {
    message.error("Đăng nhập thất bại");
  };

  const [oderRoomInfo, setOrderRoomInfo] = useState([]);

  useEffect(() => {
    getOrderRoom()
      .then((res) => {
        setOrderRoomInfo(res.data.content);
      })
      .catch((err) => {
        return {};
      });
  }, []);
  console.log("oderRoomInfo: ", oderRoomInfo);

  const checkInDates =
    oderRoomInfo
      .filter((o) => o.maPhong === roomDetail.id)
      .map((o) => o.ngayDi) || [];
  const checkOutDates =
    oderRoomInfo
      .filter((o) => o.maPhong === roomDetail.id)
      .map((o) => o.ngayDen) || [];

  const minCheck = sortDate(checkInDates);
  const maxCheck = sortDate(checkOutDates);
  const dateMin =
    minCheck && minCheck.length ? minCheck[0] : moment(new Date()).add(-1, "d");
  const dateMax = maxCheck && maxCheck.length ? maxCheck.at(-1) : dateMin;

  const sorted = sortDate([dateMin, dateMax]);
  const disabledDates = fillDates(sorted[0], sorted[1]).map((d) => toDate(d));
  // console.log(disabledDates);

  // DATE PICKER COMPONENT
  const [startDate, setStartDate] = useState(new Date());
  const [endDate, setEndDate] = useState(new Date());

  const selectionRange = {
    startDate: startDate,
    endDate: endDate,
    key: "selection",
  };

  function handleSelect(ranges) {
    setStartDate(ranges.selection.startDate);
    setEndDate(ranges.selection.endDate);
  }

  const [isModalOpen, setIsModalOpen] = useState(false);

  // const handleSubmit = () => {
  //   const payload = {
  //     maPhong: roomDetail.id,
  //     ngayDen: formatDate(startDate),
  //     ngayDi: formatDate(endDate),
  //     soLuongKhach: guest,
  //     // maNguoiDung: maNguoiDung,
  //   };
  //   console.log("payload: ", payload);
  // };

  const orderRoomByUserInfo = {
    maPhong: roomDetail.id,
    ngayDen: formatDate(startDate),
    ngayDi: formatDate(endDate),
    soLuongKhach: guest,
    maNguoiDung: maNguoiDung,
  };
  console.log("orderRoomByUserInfo: ", orderRoomByUserInfo);

  // useEffect(() => {

  // }, []);

  return (
    <div className="w-full sm:w-1/2 lg:w-2/5">
      <div className="sticky top-28">
        <div className="bg-white shadow-xl border rounded-xl p-6 w-full lg:w-5/6 mx-auto">
          <div className="relative w-full">
            <div className="hidden md:flex justify-between items-center mb-4">
              <div>
                <span>$ </span>
                <span className="text-xl font-semibold">
                  {roomDetail.giaTien}
                </span>
                <span className="text-base">/đêm</span>
              </div>
              <div>
                <span className="text-sm font-normal">
                  <i className="fa fa-star" /> 5 .
                </span>{" "}
                <span className="underline text-sm font-normal tracking-widest mx-1">
                  60 đánh giá
                </span>
              </div>
            </div>
            <div className="flex flex-col border border-solid border-gray-400 rounded-md relative">
              <div className="flex w-full border-b border-solid border-gray-400">
                <div
                  className=" rounded-tr-md w-full p-2 cursor-pointer hover:bg-gray-100"
                  onClick={() => {
                    setIsModalOpen(!isModalOpen);
                  }}
                >
                  <div className="text-xs uppercase font-semibold">
                    Nhận phòng
                  </div>
                  <div className="m-1">{formatDate(startDate)}</div>
                </div>
                <div
                  className=" rounded-tr-md w-full p-2 cursor-pointer hover:bg-gray-100"
                  onClick={() => {
                    setIsModalOpen(!isModalOpen);
                  }}
                >
                  <div className="text-xs uppercase font-semibold">
                    Trả phòng
                  </div>
                  <div className="m-1">{formatDate(endDate)}</div>
                </div>
              </div>
              {/*  */}
              <div className="absolute left-[-95px] top-[65px]">
                {isModalOpen && (
                  <div className="date-range bg-white rounded-xl shadow-xl">
                    <div className="desc flex justify-between border-b-2">
                      <div className="date m-3 flex flex-col justify-center">
                        <h3> {differenceInDays(endDate, startDate)} đêm</h3>
                        <h6>
                          {formatDate(startDate)} đến {formatDate(endDate)}
                        </h6>
                      </div>
                      <div
                        className="bg-gray-200 flex justify-center items-center m-3 px-2 rounded-2xl text-red-600 hover:bg-red-200"
                        onClick={() => {
                          setIsModalOpen(false);
                        }}
                      >
                        Close
                      </div>
                    </div>
                    <DateRangePicker
                      showSelectionPreview={false}
                      rangeColors={["red", "black"]}
                      disabledDates={disabledDates}
                      disabledDay={() => {
                        new Date();
                      }}
                      className=""
                      ranges={[selectionRange]}
                      minDate={new Date()}
                      onChange={handleSelect}
                    />
                  </div>
                )}
              </div>

              <div className="p-2">
                <div className="uppercase text-xs font-semibold">Khách</div>
                <div className="flex justify-between items-center m-1">
                  <button
                    className="w-8 h-8 bg-gray-300 hover:bg-red-400 duration-200 rounded-xl text-white cursor-pointer"
                    onClick={() => decreaseGuest()}
                  >
                    -
                  </button>
                  <div>{guest} khách</div>
                  <button
                    className="w-8 h-8 bg-gray-300 hover:bg-red-400 duration-200 rounded-xl text-white cursor-pointer"
                    onClick={() => increaseGuest()}
                  >
                    +
                  </button>
                </div>
              </div>
            </div>
            <button
              type="button"
              className="w-full py-3  mt-3 rounded-lg text-white text-lg font-semibold"
              onClick={() => handleBooking()}
              style={{
                background:
                  "linear-gradient(to right, rgb(230, 30, 77) 0%, rgb(227, 28, 95) 50%, rgb(215, 4, 102) 100%)",
              }}
            >
              Đặt phòng
            </button>
            <div className="text-center font-normal text-gray-400 my-2">
              <span>Bạn vẫn chưa bị trừ tiền</span>
            </div>
            <div className="border-b py-2">
              <div className="flex justify-between py-1 text-base">
                <div className="underline text-gray-600">
                  $ {roomDetail.giaTien} x{" "}
                  {differenceInDays(endDate, startDate)} đêm
                </div>
                <div>
                  <span>
                    {roomDetail.giaTien * differenceInDays(endDate, startDate)}
                  </span>{" "}
                  $
                </div>
              </div>
              <div className="flex justify-between py-1 text-base">
                <div className="underline text-gray-600">Phí dịch vụ</div>
                <div>
                  <span>0</span> $
                </div>
              </div>
            </div>
            <div className="flex justify-between items-center text-lg font-semibold pt-3">
              <div>Tổng trước thuế</div>
              <div>0 $</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Booking;
