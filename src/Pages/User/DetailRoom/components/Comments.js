import React from "react";

import { Avatar, message } from "antd";
import { useDispatch } from "react-redux";
import { postComments } from "../../../../Services/userServ";

function Comments({ avatar, token }) {
  const dispatch = useDispatch();
  const commentInfo = {
    maPhong: 17,
    maNguoiBinhLuan: 2698,
    ngayBinhLuan: "10/09/2019",
    noiDung: "alibabaa",
    saoBinhLuan: 5,
  };

  const handleSubmit = () => {
    dispatch(postComments(commentInfo, token, onSuccess, onError));
  };
  let onSuccess = () => {
    message.success("Đặt phòng thành công");
  };
  let onError = () => {
    message.error("Đăng nhập thất bại");
  };

  return (
    <div className="max-w-2xl mx-auto px-4">
      <div className="flex justify-between items-center mb-6">
        <h2 className="text-lg lg:text-2xl font-bold text-gray-900 dark:text-white">
          Discussion (20)
        </h2>
      </div>
      <Avatar src={avatar} alt="avatar" />
      <form className="mb-6 mt-2">
        <div className="py-2 px-4 mb-4 bg-white rounded-lg rounded-t-lg border border-gray-200 dark:bg-gray-800 dark:border-gray-700">
          <label htmlFor="comment" className="sr-only">
            Your comment
          </label>
          <textarea
            id="comment"
            rows={6}
            className="px-0 w-full text-sm text-gray-900 border-0 focus:ring-0 focus:outline-none dark:text-white dark:placeholder-gray-400 dark:bg-gray-800"
            placeholder="Write a comment..."
            required
            defaultValue={""}
          />
        </div>
        <div className="text-center flex !items-center justify-center">
          <button
            className="inline-flex py-2.5 px-4 text-xs font-medium !text-center text-black bg-primary-700 rounded-lg focus:ring-4 focus:ring-primary-200 dark:focus:ring-primary-900 hover:bg-primary-800"
            onClick={() => {
              handleSubmit();
            }}
          >
            Post commentssss
          </button>
        </div>
      </form>
    </div>
  );
}

export default Comments;
