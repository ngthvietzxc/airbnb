import React from "react";

function Room() {
  return (
    <div className="border-b-2 pb-3 relative pt-3">
      <div className="overflow-hidden flex">
        <div className="">
          <img
            src="https://airbnb.cybersoft.edu.vn/public/images/room/1658417426651_dirtiest-1170x650.jpg"
            alt="tenphong"
            className="block w-full rounded-[1rem] h-[280px] object-cover"
          />
          <button className="absolute top-3 right-3 z-30">
            <svg
              viewBox="0 0 32 32"
              xmlns="http://www.w3.org/2000/svg"
              aria-hidden="true"
              role="presentation"
              focusable="false"
              style={{
                display: "block",
                fill: "rgba(0, 0, 0, 0.5)",
                height: 24,
                width: 24,
                stroke: "rgb(255, 255, 255)",
                strokeWidth: 2,
                overflow: "hidden",
              }}>
              <path d="m16 28c7-4.733 14-10 14-17 0-1.792-.683-3.583-2.05-4.95-1.367-1.366-3.158-2.05-4.95-2.05-1.791 0-3.583.684-4.949 2.05l-2.051 2.051-2.05-2.051c-1.367-1.366-3.158-2.05-4.95-2.05-1.791 0-3.583.684-4.949 2.05-1.367 1.367-2.051 3.158-2.051 4.95 0 7 7 12.267 14 17z" />
            </svg>
          </button>
        </div>
        <div className="ml-4">
          <p className="font-bold  text-ellipsis overflow-hidden whitespace-nowrap mt-2">
            Toàn bộ các căn hộ ở bình thạnh
          </p>
          <p className="text-gray-500 text-ellipsis overflow-hidden whitespace-nowrap">
            Romantic APT for longtime
          </p>

          <p className="text-gray-500">
            <span>2 khách</span>-<span>Phòng Studio</span>-<span>1 giường</span>
            -<span>1 phòng tắm</span>-<span>Wifi</span>-<span>Bếp</span>-
            <span>Điều hoà nhiệt độ</span>-<span>Máy giặt</span>
          </p>
          <p className="mt-1 flex justify-end">
            <span className="font-bold">950000 VNĐ</span> đêm
          </p>
        </div>
      </div>
    </div>
  );
}

export default Room;
