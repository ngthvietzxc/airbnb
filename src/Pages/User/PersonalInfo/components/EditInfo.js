import React from "react";
import { formatDate } from "../../../../lib/date";
import { message } from "antd";
import { putEditUserInfo } from "../../../../Services/userServ";
import Stack from "@mui/material/Stack";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import { useForm } from "react-hook-form";
import { MenuItem } from "@mui/material";

function EditInfo({ userLoginInfo, setOpen }) {
  const userID = userLoginInfo.id;

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const genderSelect = [
    {
      value: "true",
      label: "Nam",
    },
    {
      value: "false",
      label: "Nữ",
    },
  ];

  return (
    <div className=" w-full flex justify-center items-center ">
      <Stack spacing={2} sx={{ width: "100%" }}>
        <form
          onSubmit={handleSubmit((values) => {
            putEditUserInfo(values, userID)
              .then((res) => {
                message.success("Chỉnh sửa thông tin thành công");
                setOpen(false);
              })
              .catch((err) => {
                message.error("Chỉnh sửa thông tin thất bại");
              });
          })}
          className="bg-white w-3/4 shadow-md rounded px-8 pt-6 pb-[80px] mb-4">
          <div className="mb-6">
            <TextField
              {...register("name", {
                required: "Name is required.",
                minLength: {
                  value: 6,
                  message: "Password must greater than 6 characters",
                },
              })}
              size="medium"
              style={{ width: 400 }}
              id="name"
              label="Name"
              type="text"
              defaultValue={userLoginInfo.name}
            />
            <p className="text-red-500 text-xs italic mt-3 text-left">
              {errors.name?.message}
            </p>
          </div>
          <div className="w-full mb-4">
            <TextField
              {...register("email", {
                required: "Email is required.",
                pattern: {
                  value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                  message: "Email must be a valid email",
                },
              })}
              size="medium"
              style={{ width: 400 }}
              id="email"
              type="email"
              label="Email"
              defaultValue={userLoginInfo.email}
            />
            <p className="text-red-500 text-xs italic mt-3 text-left">
              {errors.email?.message}
            </p>
          </div>
          <div className="mb-6">
            <TextField
              {...register("phone", {
                required: "Phone is required.",
                Length: {
                  value: 10,
                  message: "Phone must greater than 6 characters",
                },
              })}
              size="medium"
              style={{ width: 400 }}
              id="phone"
              label="Số điện thoại"
              type="phone"
              defaultValue={userLoginInfo.phone}
            />
            <p className="text-red-500 text-xs italic mt-3 text-left">
              {errors.phone?.message}
            </p>
          </div>
          <div className="mb-6">
            <TextField
              {...register("gender", {
                required: "Gender is required.",
              })}
              size="medium"
              style={{ width: 400 }}
              id="gender"
              select
              label="Giới tính"
              defaultValue={userLoginInfo.gender ? "true" : "false"}
              helperText="Please select your gender">
              {genderSelect.map((option) => (
                <MenuItem key={option.value} value={option.value}>
                  {option.label}
                </MenuItem>
              ))}
            </TextField>
            <p className="text-red-500 text-xs italic mt-3 text-left">
              {errors.gender?.message}
            </p>
          </div>
          <div className="mb-6">
            <TextField
              {...register("birthday", {
                required: "Birthday is required.",
                minLength: {
                  value: 6,
                  message: "Password must greater than 6 characters",
                },
              })}
              size="medium"
              style={{ width: 400 }}
              id="birthday"
              label="Ngày sinh"
              type="date"
              defaultValue={formatDate(userLoginInfo.birthday)}
            />
            <p className="text-red-500 text-xs italic mt-3 text-left">
              {errors.birthday?.message}
            </p>
          </div>

          <Button variant="contained" type="submit" color="error">
            Edit
          </Button>
        </form>
      </Stack>
    </div>
  );
}

export default EditInfo;
