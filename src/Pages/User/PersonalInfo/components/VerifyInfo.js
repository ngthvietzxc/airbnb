import React from "react";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";

import Modal from "@mui/material/Modal";
import TextField from "@mui/material/TextField";
import Stack from "@mui/material/Stack";
import { useForm } from "react-hook-form";
import { postUserAvatar } from "../../../../Services/userServ";
import { message } from "antd";
import { useSelector } from "react-redux";
import { Avatar } from "@mui/material";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 600,
};
function VerifyInfo({ userLoginInfo }) {
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const userSingIn = useSelector((state) => state.userLoginReducer.userSingIn);

  //  const userLoginInfo = userSingIn?.user;

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();
  return (
    <div className="w-full sticky border rounded-lg h-[500px] px-5 ml-8 flex flex-col justify-center items-center">
      <div className="space-y-4">
        <Avatar
          className="mx-auto rounded-full mb-4"
          src={
            userLoginInfo !== ""
              ? userLoginInfo.avatar
              : "https://a0.muscache.com/defaults/user_pic-50x50.png?v=3"
          }
          style={{ width: 129, height: 129 }}
        />
        <Button
          className="mt-5"
          variant="contained"
          color="error"
          onClick={handleOpen}
        >
          Chỉnh sửa avatar
        </Button>
        <Modal
          open={open}
          onClose={handleClose}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        >
          <Box sx={style}>
            <Stack spacing={2} sx={{ width: "100%" }}>
              <form
                onSubmit={handleSubmit((values) => {
                  const payload = {
                    ...values,
                    avatar: values.avatar[0],
                  };
                  // console.log("valuestung: ", payload);

                  postUserAvatar(payload)
                    .then((res) => {
                      message.success("Chỉnh sửa thông tin thành công");
                      setOpen(false);
                    })
                    .catch((err) => {
                      message.error("Chỉnh sửa thông tin thất bại");
                    });
                })}
                className="bg-white w-3/4 shadow-md rounded px-8 pt-6 pb-[80px] mb-4"
              >
                <div className="mb-6">
                  <TextField
                    {...register("avatar")}
                    size="medium"
                    style={{ width: 400 }}
                    id="name"
                    type="file"
                    helperText="Vui lòng nhập url hình ảnh đại diện"
                  />
                  <p className="text-red-500 text-xs italic mt-3 text-left">
                    {/* {errors.name?.message} */}
                  </p>
                </div>

                <div className="my-5">
                  <Button variant="contained" type="submit" color="error">
                    Cập nhật
                  </Button>
                </div>
              </form>
            </Stack>
          </Box>
        </Modal>
      </div>
      <div className="mt-2">
        <div className="flex items-center">
          <img
            src="https://www.pngmart.com/files/12/Instagram-Verified-Badge-PNG-File.png"
            alt=""
            width="30px"
          />
          <span className="ml-2 font-semibold text-lg">Xác minh danh tính</span>
        </div>
        <div className="flex flex-col justify-center items-center">
          <p className="text-gray-600 py-1 text-base text-center">
            Xác minh danh tính của bạn với huy hiệu xác minh danh tính.
          </p>

          <div className="my-5">
            <Button variant="contained" color="error">
              Nhận huy hiệu
            </Button>
          </div>
        </div>
      </div>
      <div className="mt-2 border-t py-2">
        <div className="font-semibold text-lg text-gray-800">Đã xác nhận</div>
        <div className="mt-2">
          <i className="fa-solid fa-check" />
          <span className="ml-2 text-sm italic">Địa chỉ email</span>
        </div>
      </div>
    </div>
  );
}

export default VerifyInfo;
