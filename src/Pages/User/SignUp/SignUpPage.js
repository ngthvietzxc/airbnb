import React, { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { message } from "antd";

import { Button, Form, Input, Select, DatePicker } from "antd";
import { setUserSignUpAction } from "../../../redux/action/userSignUpAction";

const { Option } = Select;

const formItemLayout = {
  labelCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 8,
    },
  },
  wrapperCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 16,
    },
  },
};

function SignUpPage() {
  const [gender, setGender] = useState(true);
  const dispatch = useDispatch();
  let navigate = useNavigate();

  // form antd
  const [form] = Form.useForm();
  const onFinish = (values) => {
    let onSuccess = () => {
      message.success("Đăng ký thành công, chuyển về trang đăng nhập!");
      navigate("/sign-in");
    };
    let onError = () => {
      message.error("Đăng nhập thất bại");
    };
    console.log("values: ", values);

    dispatch(setUserSignUpAction(values, onSuccess, onError));

    console.log("Success:", values);
    console.log("Received values of form: ", values);
  };

  return (
    <div
      className="w-screen h-screen relative mt-[100px]"
      style={{
        backgroundImage:
          'url("http://demo4.cybersoft.edu.vn/static/media/logo_login.a444f2681cc7b623ead2.jpg")',
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
      }}
    >
      <div className="w-full p-5">
        <div className="md:absolute top-1/2 left-1/2 md:-translate-x-1/2 md:-translate-y-1/2 border rounded-lg shadow-lg bg-white px-10 py-5 w-4/5 sm:w-2/3 mx-auto">
          <Form
            {...formItemLayout}
            form={form}
            name="register"
            onFinish={onFinish}
            scrollToFirstError
          >
            <Form.Item
              name="name"
              label="Name:"
              tooltip="Tên của bạn là gì?"
              rules={[
                {
                  required: true,
                  message: "Vui lòng gõ vào tên của bạn!",
                  whitespace: true,
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              name="email"
              label="E-mail"
              rules={[
                {
                  type: "email",
                  message: "Email không hợp lệ!",
                },
                {
                  required: true,
                  message: "Vui lòng điền Email của bạn!",
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              name="password"
              label="Password"
              rules={[
                {
                  required: true,
                  message: "Vui lòng điền mật khẩu!",
                },
              ]}
              hasFeedback
            >
              <Input.Password />
            </Form.Item>

            <Form.Item
              name="phone"
              label="Phone Number"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập số điện thoại của bạn!",
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              name="birthday"
              label="Birthday"
              tooltip="DD/MM/YYYY"
              rules={[
                {
                  type: "date",
                  message: "Ngày tháng không hợp lệ!",
                },
                {
                  required: true,
                  message: "Vui lòng điền ngày sinh của bạn!",
                },
              ]}
            >
              <DatePicker showTime={false} format="DD/MM/YYYY" />
            </Form.Item>

            <Form.Item
              name="gender"
              label="Gender"
              rules={[
                {
                  required: true,
                  message: "Vui lòng chọn giới tính!",
                },
              ]}
            >
              <Select placeholder="Chọn giới tính của bạn">
                <Option value={true}>Nam</Option>
                <Option value={false}>Nữ</Option>
              </Select>
            </Form.Item>

            <Form.Item name="role" label="Vai trò" tooltip="User hay Admin">
              <Select placeholder="Chọn vai trò của bạn">
                <Option value="user">User</Option>
                <Option value="admin">Admin</Option>
              </Select>
            </Form.Item>

            <div className="grid items-center justify-center ">
              <Form.Item>
                <Link to="/sign-in">
                  {" "}
                  <p>Đăng nhập</p>
                </Link>
              </Form.Item>
              <Form.Item>
                <Button type="primary" htmlType="submit" className="bg-red-500">
                  Đăng ký
                </Button>
              </Form.Item>
            </div>
          </Form>
        </div>
      </div>
    </div>
  );
}
export default SignUpPage;
