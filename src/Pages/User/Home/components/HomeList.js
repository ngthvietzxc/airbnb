import React, { useEffect, useState } from "react";
import HomeItem from "./HomeItem";
import { getHomeList } from "../../../Services/product.service";

export default function HomeList() {
  const [homeList, setHomeList] = useState([]);

  useEffect(() => {
    getHomeList().then((data) => setHomeList(data));
  }, []);

  return (
    <div className="container mx-auto">
      <div className="grid grid-cols-4 gap-6">
        {homeList &&
          homeList.map((item, index) => (
            <div key={item._id} data_id={item._id} className="mb-3">
              <HomeItem {...item} />
            </div>
          ))}
      </div>
    </div>
  );
}
