import React from "react";
import { Space, Typography } from "antd";
import { Link } from "react-router-dom";

function HomeBannerBottom() {
  const list = [
    {
      hinhAnh:
        "https://kovisan.com/wp-content/uploads/2022/06/nha-go-san-vuon-mang-lai-khong-gian-song-thu-thai-de-chiu-e1654162944846_1660040092.jpg",
      desc: "Toàn bộ nhà",
    },
    {
      hinhAnh:
        "https://static1.cafeland.vn/cafelandnew/hinh-anh/2020/10/09/153/kla19.jpg",
      desc: "Chỗ ở độc đáo",
    },
    {
      hinhAnh:
        "https://1.bp.blogspot.com/-N5Ld9hu8Xsw/XQs1g5y3WoI/AAAAAAAAEEY/GoPn34WaZJMkN1KKhfAbe_oSTN9s8UgbwCLcBGAs/s1600/Beach-Plum-Farm-My-1.jpg",
      desc: "Trang trại và thiên nhiên",
    },
    {
      hinhAnh:
        "https://images2.thanhnien.vn/528068263637045248/2023/4/6/2-cho-meo-shutterstock-1680795958168987499640.jpg",
      desc: "Cho phép mang theo thú cưng",
    },
  ];
  return (
    <Space className="container mx-auto flex flex-col justify-center items-center text-center mt-20  ">
      <Typography.Title className="pb-10">Ở bất kỳ đâu</Typography.Title>
      <div className="grid sm:grid-cols-2 xl:grid-cols-4 md:gap-20">
        {list.map((item, index) => {
          return (
            <Link className="" to="#" key={index}>
              <div className=" mr-5   hover:translate-y-[-10px] transition">
                <img
                  src={item.hinhAnh}
                  alt={item.desc}
                  className="rounded-2xl mb-2"
                  style={{
                    height: 300,
                    width: 300,
                    objectFit: "cover",
                    objectPosition: "center",
                  }}
                />
                <Typography.Paragraph className="mr-5 font-bold">
                  {item.desc}
                </Typography.Paragraph>
              </div>
            </Link>
          );
        })}
      </div>
    </Space>
  );
}

export default HomeBannerBottom;
