import React from "react";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import { Link } from "react-router-dom";
var Carousel = require("react-responsive-carousel").Carousel;

export default function HomeItemSlider({ images }) {
  const renderPrevArrow = (callback, hasPrev) => (
    <button
      onClick={callback}
      disabled={!hasPrev}
      className="slider-button left-4 bg-white">
      <ChevronLeftIcon className={`${hasPrev ? "" : "text-[#eee]"}`} />
    </button>
  );

  const renderNextArrow = (callback, hasNext) => (
    <button
      onClick={callback}
      disabled={!hasNext}
      className="slider-button right-4 bg-white">
      <ChevronRightIcon className={`${hasNext ? "" : "text-[#eee]"}`} />
    </button>
  );

  return (
    <div className="container mx-auto">
      <Carousel
        showArrows={true}
        swipeable={true}
        emulateTouch
        showThumbs={false}
        showStatus={false}
        showIndicators={true}
        renderArrowPrev={renderPrevArrow}
        renderArrowNext={renderNextArrow}>
        {images.map((src, index) => (
          <Link to="#" className="block no-underline">
            <div
              key={index}
              className="select-none min-h-[80px] flex flex-col items-center justify-center rounded-2xl overflow-hidden">
              <img src={src} className="block h-[280px] !w-full rounded-2xl" />
            </div>
          </Link>
        ))}
      </Carousel>
    </div>
  );
}
