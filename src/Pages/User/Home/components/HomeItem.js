import React from "react";
import StarIcon from "@mui/icons-material/Star";
import FavoriteIcon from "@mui/icons-material/Favorite";
import util from "./../../../utils/util";
import HomeItemSlider from "./HomeItemSlider";
import { Link } from "react-router-dom";

export default function HomeItem({ name, image, ...rest }) {
  return (
    <div className="overflow-hidden relative">
      <Link to="#" className="absolute top-4 right-4 z-10">
        <FavoriteIcon color="error" />
      </Link>
      <div>
        {/* <img
          src={image}
          alt=""
          className="block w-full rounded-[1rem] h-[280px] object-cover"
        /> */}
        <HomeItemSlider images={[image, image, image]} />
      </div>

      <div className="flex justify-between font-[600] mt-3">
        <h4>{name}</h4>
        <div className="flex items-center gap-1">
          <StarIcon fontSize="small" />
          <span>{util.randomF(1, 10, 2)}</span>
        </div>
      </div>

      <div className="text-sm text-slate-400">{util.randomI(10, 9999)} km</div>
      <div className="text-sm text-slate-400">
        Ngày {util.randomI(1, 30)} - Ngày {util.randomI(1, 30)} tháng{" "}
        {util.randomI(1, 12)}
      </div>
      <div>
        <b>${util.randomI(10, 1000)}</b> đêm
      </div>
    </div>
  );
}
