import React from "react";
// import HomeSlider from './components/Slider';
// import HomeList from './components/HomeList';
import HomeBannerTop from "./components/HomeBannerTop";
import ProvinceList from "./components/ProvinceList";
import HomeBannerBottom from "./components/HomeBannerBottom";

function HomePage() {
  return (
    <div className="home">
      <HomeBannerTop />
      <ProvinceList />
      <HomeBannerBottom />
    </div>
  );
}

export default HomePage;
