import React, { useState } from 'react';
import './Banner.css';
import { Button } from '@mui/material';
import Search from '../Search/Search';

function Banner() {
  const [showSearch, setShowShearch] = useState(false);
  return (
    <div className="banner">
      <div className="banner__search">
        {showSearch && <Search />}
        <Button
          variant="outlined"
          className="banner__searchButton"
          onClick={() => setShowShearch(!showSearch)}>
          {showSearch ? 'Hide' : 'Search Dates'}
        </Button>
      </div>
      <div className="banner__infor">
        <h1>Get out and stretch your imagination</h1>
        <h5>
          Plan a different kind of gateway to uncover the hidden gems near you.
        </h5>
        <Button variant="outlined">Explore Nearby</Button>
      </div>
    </div>
  );
}

export default Banner;
