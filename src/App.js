import { BrowserRouter, Route, Routes } from "react-router-dom";
import "./App.css";
import Login from "./Pages/Admin/Login/Login";
import DetailRoomPage from "./Pages/User/DetailRoom/DetailRoomPage";
import HomePage from "./Pages/User/Home/HomePage";
import PersonalInfo from "./Pages/User/PersonalInfo/PersonalInfo";
import RoomByLocation from "./Pages/User/RoomByLocation/RoomByLocationPage";
import SignUpPage from "./Pages/User/SignUp/SignUpPage";
import SignInPage from "./Pages/User/SingIn/SignInPage";
import AdminTemplate from "./Template/AdminTemplate/AdminTemplate";
import UserTemplate from "./Template/UserTemplate/UserTemplate";
import Dashboard from "./Pages/Admin/Dashboard/Dashboard";
import UserManage from "./Pages/Admin/UserManage/UserManage.jsx";
import EditUser from "./Pages/Admin/UserManage/EditUser.jsx";
import LocationManage from "./Pages/Admin/LocationManage/LocationManage.jsx";
import EditLocation from "./Pages/Admin/LocationManage/EditLocation.jsx";
import RoomManage from "./Pages/Admin/RoomManage/RoomManage";
import EditRoom from "./Pages/Admin/RoomManage/EditRoom";
import BookingManage from "./Pages/Admin/BookingManage/BookingManage";
import EditBooking from "./Pages/Admin/BookingManage/EditBooking";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          {/* User Section */}
          <Route path="/" element={<UserTemplate Component={HomePage} />} />
          <Route
            path="/room-by-location/:id"
            element={<UserTemplate Component={RoomByLocation} />}
          />
          <Route
            path="/detail-room/:id"
            element={<UserTemplate Component={DetailRoomPage} />}
          />
          <Route
            path="/personal-info"
            element={<UserTemplate Component={PersonalInfo} />}
          />
          <Route path="/sign-in" element={<SignInPage />} />
          <Route path="/sign-up" element={<SignUpPage />} />

          {/* Admin Section */}

          <Route path="/admin" element={<Login />} />

          <Route
            path="/admin/dashboard"
            element={<AdminTemplate Component={Dashboard} />}
          />

          <Route
            path="/admin/users"
            element={<AdminTemplate Component={UserManage} />}
          />

          <Route
            path="/admin/users/:id"
            element={<AdminTemplate Component={EditUser} />}
          />

          <Route
            path="/admin/locations"
            element={<AdminTemplate Component={LocationManage} />}
          />

          <Route
            path="/admin/locations/:id"
            element={<AdminTemplate Component={EditLocation} />}
          />

          <Route
            path="/admin/rooms"
            element={<AdminTemplate Component={RoomManage} />}
          />

          <Route
            path="/admin/rooms/:id"
            element={<AdminTemplate Component={EditRoom} />}
          />

          <Route
            path="/admin/bookings"
            element={<AdminTemplate Component={BookingManage} />}
          />

          <Route
            path="/admin/bookings/:id"
            element={<AdminTemplate Component={EditBooking} />}
          />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
