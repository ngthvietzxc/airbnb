import axios from "axios";
import { BASE_URL, configHeaders } from "./config";

export const userService = {
  postLogin: (formLogin) => {
    return axios({
      url: `${BASE_URL}/api/auth/signin`,
      method: "POST",
      data: formLogin,
      headers: configHeaders(),
    });
  },
};
