import axios from "axios";
import { BASE_URL, configHeaders } from "./config";
// import { localUserService } from "./localService";

import { localAdminService } from "./localServ";

export const locationManageService = {
  getLocation: () => {
    return axios({
      url: `${BASE_URL}/api/vi-tri`,
      method: "GET",
      headers: configHeaders(),
    });
  },
  addLocation: (locationAdd) => {
    const token = localAdminService.get()?.token;
    console.log(token);
    return axios({
      url: `${BASE_URL}/api/vi-tri`,
      method: "POST",
      data: locationAdd,
      headers: {
        ...configHeaders(),
        token: token,
      },
    });
  },
  deleteLocation: (id) => {
    const token = localAdminService.get()?.token;
    return axios({
      url: `${BASE_URL}/api/vi-tri/${id}`,
      method: "DELETE",
      headers: {
        ...configHeaders(),
        token: token,
      },
    });
  },
  getLocationById: (id) => {
    return axios({
      url: `${BASE_URL}/api/vi-tri/${id}`,
      method: "GET",
      headers: configHeaders(),
    });
  },
  putLocationById: (id, locationDataById) => {
    const token = localAdminService.get()?.token;
    return axios({
      url: `${BASE_URL}/api/vi-tri/${id}`,
      method: "PUT",
      data: locationDataById,
      headers: {
        ...configHeaders(),
        token: token,
      },
    });
  },
};
