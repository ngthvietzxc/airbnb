import axios from "axios";
import { BASE_URL, configHeaders } from "../utils/apiURL";

export const getOrderRoom = () => {
  const response = axios.get(`${BASE_URL}/api/dat-phong`, {
    headers: configHeaders(),
  });

  return response;
};

export const postOderRoom = (orderInfo) => {
  const response = axios.post(`${BASE_URL}/api/dat-phong`, orderInfo, {
    headers: configHeaders(),
  });

  return response;
};
