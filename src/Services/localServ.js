// User
const USER_LOGIN = "USER_LOGIN";

export const localUserServ = {
  get: () => {
    let dataJson = localStorage.getItem(USER_LOGIN);
    return JSON.parse(dataJson);
  },
  set: (userInfo) => {
    let dataJson = JSON.stringify(userInfo);
    localStorage.setItem(USER_LOGIN, dataJson);
  },
  remove: () => {
    localStorage.removeItem(USER_LOGIN);
  },
};

// Admin
const ADMIN_LOGIN = "ADMIN_LOGIN";

export const localAdminService = {
  get: () => {
    let dataJson = localStorage.getItem(ADMIN_LOGIN);
    return JSON.parse(dataJson);
  },
  set: (adminInfo) => {
    let dataJson = JSON.stringify(adminInfo);
    localStorage.setItem(ADMIN_LOGIN, dataJson);
  },
  remove: () => {
    localStorage.removeItem(ADMIN_LOGIN);
  },
};
