import axios from "axios";
import { BASE_URL, configHeaders } from "./config";
// import { localUserService } from "./localService";
import { localAdminService } from "./localServ";

export const bookingManageService = {
  getBooking: () => {
    return axios({
      url: `${BASE_URL}/api/dat-phong`,
      method: "GET",
      headers: configHeaders(),
    });
  },
  deleteBooking: (id) => {
    const token = localAdminService.get()?.token;
    return axios({
      url: `${BASE_URL}/api/dat-phong/${id}`,
      method: "DELETE",
      headers: {
        ...configHeaders(),
        token: token,
      },
    });
  },
  addBooking: (bookingAdd) => {
    return axios({
      url: `${BASE_URL}/api/dat-phong`,
      method: "POST",
      data: bookingAdd,
      headers: configHeaders(),
    });
  },
  getBookingById: (id) => {
    return axios({
      url: `${BASE_URL}/api/dat-phong/${id}`,
      method: "GET",
      headers: configHeaders(),
    });
  },
  putBookingById: (id, bookingDataById) => {
    return axios({
      url: `${BASE_URL}/api/dat-phong/${id}`,
      method: "PUT",
      data: bookingDataById,
      headers: configHeaders(),
    });
  },
};
