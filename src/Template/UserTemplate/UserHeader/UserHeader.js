import React, { useEffect, useState } from "react";
import "./UserHeader.css";
import SearchIcon from "@mui/icons-material/Search";
import { Button } from "@mui/material";
import LanguageIcon from "@mui/icons-material/Language";
import MenuIcon from "@mui/icons-material/Menu";
import AccountCircleIcon from "@mui/icons-material/AccountCircle";
import { Collapse } from "@mui/material";
import { getLocationList } from "../../../Services/locationServ";
import { Link, useNavigate } from "react-router-dom";
import SubMenu from "../../../Components/SubMenu/SubMenu";
import { useSelector } from "react-redux";
import { Avatar } from "antd";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import Backdrop from "@mui/material/Backdrop";
import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";
import Fade from "@mui/material/Fade";

const style = {
  position: "absolute",
  top: "30%",
  left: "85%",
  transform: "translate(-50%, -50%)",
  width: 300,
};

function UserHeader() {
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  const navigate = useNavigate();
  const [locationList, setLocationList] = useState([]);
  console.log("locationList: ", locationList);

  const userSingIn = useSelector((state) => state.userLoginReducer.userSingIn);

  const userLoginInfo = userSingIn?.user;
  console.log("userLoginInfo: ", userLoginInfo);

  useEffect(() => {
    getLocationList()
      .then((res) => {
        setLocationList(res.data.content);
      })
      .catch((err) => {
        return {};
      });
  }, []);

  const [locationId, setLocationId] = useState(null);
  const handleSubmit = () => {
    locationId !== null && navigate(`/room-by-location/${locationId}`);
  };

  const [isSearchExpanded, setSearchExpanded] = useState(false);

  return (
    <div
      className={`border-b${
        isSearchExpanded ? " shadow-md" : ""
      }  sticky top-0  w-full z-20 bg-white`}>
      <header className="container mx-auto mb-3 ">
        <div className="flex items-center">
          <Link to="/" className="block no-underline">
            <img
              className="h-[64px]"
              src="https://1000logos.net/wp-content/uploads/2023/01/Airbnb-logo-768x432.png"
              alt="icon-airbnb"
            />
          </Link>
          {!isSearchExpanded && (
            <div className="flex-1">
              <div className="flex-1 flex justify-center">
                <div
                  className=" flex items-center gap-3 border rounded-full py-1 text-sm cursor-pointer"
                  onClick={() => setSearchExpanded(!isSearchExpanded)}>
                  <div
                    className="px-2 border-r
                  ">
                    Địa điểm bất kỳ
                  </div>
                  <div
                    className="px-2 border-r
                  ">
                    Tuần bất kỳ
                  </div>
                  <div>Thêm khách</div>

                  <div className="flex justify-center items-center bg-red-600 rounded-full text-white p-1 mr-2 ">
                    <SearchIcon className="!text[1rem]" />
                  </div>
                </div>
              </div>
            </div>
          )}

          {isSearchExpanded && (
            <div className="flex-1">
              <div className="flex flex-1 justify-center">
                <div
                  className=" flex items-center gap-3 border rounded-full py-1 text-sm cursor-pointer"
                  onClick={() => setSearchExpanded(!isSearchExpanded)}>
                  <div className="px-2 border-r">Chỗ ở</div>
                  <div className="px-2 border-r">Trải nghiệm</div>
                  <div className="px-2 ">Trải nghiệm trực tuyến</div>
                </div>
              </div>
            </div>
          )}
          <div className="flex items-center gap-4">
            <div className="hover:bg-slate-300 px-2 py-2 rounded-full">
              Trở thành chủ nhà
            </div>

            <div className="hover:bg-slate-300 px-2 py-1 rounded-full">
              <LanguageIcon />
            </div>

            <div>
              <button
                className="border px-2 py-1 gap-2 rounded-full flex items-center hover:shadow-md "
                onClick={handleOpen}>
                <MenuIcon />
                {userLoginInfo ? (
                  <Avatar src={userLoginInfo?.avatar} />
                ) : (
                  <AccountCircleIcon fontSize="large" />
                )}
              </button>
              <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                open={open}
                onClose={handleClose}
                closeAfterTransition
                slots={{ backdrop: Backdrop }}
                slotProps={{
                  backdrop: {
                    timeout: 500,
                  },
                }}>
                <Fade in={open}>
                  <Box sx={style}>
                    <SubMenu open={open} setOpen={setOpen} />
                  </Box>
                </Fade>
              </Modal>
            </div>
          </div>
        </div>

        <div className="flex justify-center ">
          <Collapse in={isSearchExpanded} orientation="vertical">
            <div className="flex  bg-slate-300w  transition-all duration-300 h-16 rounded-full border search-box">
              <div className="px-5 py-2 hover:bg-gray-300 rounded-full h-full flex flex-wrap justify-center items-center">
                <label
                  htmlFor="checkInDate"
                  className="block text-sm font-medium text-gray-900 dark:text-gray-300 mr-2">
                  Địa điểm
                </label>
                <FormControl
                  className="border-none"
                  variant="standard"
                  sx={{ m: 1, minWidth: 120 }}>
                  <Select
                    className="border-none"
                    defaultValue={""}
                    onChange={(e) => {
                      setLocationId(e.target.value);
                    }}
                    displayEmpty
                    inputProps={{ "aria-label": "Without label" }}>
                    <MenuItem value="" disabled>
                      <em>Chọn địa điểm đến</em>
                    </MenuItem>
                    {locationList.map((local, index) => {
                      return (
                        <MenuItem value={local.id} key={index}>
                          {local.tenViTri} , {local.tinhThanh} , {local.quocGia}
                        </MenuItem>
                      );
                    })}
                  </Select>
                </FormControl>
              </div>

              <div className="hidden sm:block py-2 px-5 hover:bg-gray-300 rounded-full overflow-hidden h-full">
                <label
                  className="block text-sm font-medium text-gray-900 dark:text-gray-300"
                  htmlFor="checkInDate">
                  Nhận phòng
                </label>
                <input
                  type="date"
                  name="checkIn"
                  id="checkInDate"
                  className="bg-transparent outline-none"
                />
              </div>

              <div className="hidden sm:block py-2 px-5 hover:bg-gray-300 rounded-full overflow-hidden h-full">
                <label
                  className="block text-sm font-medium text-gray-900 dark:text-gray-300"
                  htmlFor="checkOutDate">
                  Trả phòng
                </label>
                <input
                  type="date"
                  name="checkOut"
                  id="checkOutDate"
                  className="bg-transparent outline-none"
                />
              </div>

              <div className=" flex justify-center items-center  py-2 pl-7 pr-5 hover:bg-gray-300 rounded-full overflow-hidden h-full">
                <div className="mr-6">
                  <label
                    htmlFor="guest"
                    className="block text-sm font-medium text-gray-900 dark:text-gray-300">
                    Khách
                  </label>
                  <input
                    type="number"
                    name="guest"
                    id="guest"
                    placeholder="Thêm khách"
                    className="number-text max-w-[100px] bg-transparent outline-none"
                  />
                </div>
                <>
                  <Button
                    variant="contained"
                    color="error"
                    className="!rounded-full !normal-case"
                    size="large"
                    onClick={() => handleSubmit()}>
                    Tìm kiếm
                  </Button>
                </>
              </div>
            </div>
          </Collapse>
        </div>
      </header>
    </div>
  );
}

export default UserHeader;
