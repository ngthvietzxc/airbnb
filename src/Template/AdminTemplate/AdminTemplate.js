import React from "react";
import AdminHeader from "./AdminHeader";
// import SideBar from "./Components/SideBar/SideBar";
import SideBar from "../../Components/SideBar/SideBar";
// import sidebar_menu from "./redux/constant/SlideBarConstant";
import sidebar_menu from "../../redux//constant/SlideBarConstant";

function AdminTemplate({ Component }) {
  return (
    <div className="h-screen">
      <div className="flex flex-row">
        <SideBar menu={sidebar_menu} />
        <div className="flex-column flex-1">
          <AdminHeader className="z-40" />
          <Component />
        </div>
      </div>
    </div>
  );
}

export default AdminTemplate;
