import { combineReducers } from "redux";
import userReducer from "./userReducer";
import { userManageReducer } from "./userManageReducer";
import { locationManageReducer } from "./locationManageReducer";
import { roomManageReducer } from "./roomManageReducer";
import { bookingManageReducer } from "./bookingManageReducer";
//user
import userLoginReducer from "./userLoginReducer";
import userSignUpReducer from "./userSignUpReducer";
import userOrderRoomReducer from "./userOrderRoomReducer";
import userCommentReducer from "./userCommentReducer";

export const rootReducer = combineReducers({
  userReducer,
  userManageReducer,
  locationManageReducer,
  roomManageReducer,
  bookingManageReducer,
  // user
  userLoginReducer,
  userSignUpReducer,
  userOrderRoomReducer,
  userCommentReducer,
});
