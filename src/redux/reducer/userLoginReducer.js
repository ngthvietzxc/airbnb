import { localUserServ } from "../../Services/localServ";
import { SET_USER_LOGIN, SET_USER_SIGNUP } from "../constant/userConstant";

const initialState = {
  userSingIn: localUserServ.get(),
};

const userLoginReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_USER_LOGIN:
      state.userInfor = action.userInfor;
      return { ...state };

    default:
      return state;
  }
};

export default userLoginReducer;
