import { localUserServ } from "../../Services/localServ";
import { SET_USER_LOGIN, SET_USER_SIGNUP } from "../constant/userConstant";

const initialState = {
  userSignUp: [],
};

const userSignUpReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_USER_SIGNUP:
      state.userSignUp = action.userSignUp;
      return { ...state };

    default:
      return state;
  }
};

export default userSignUpReducer;
