import { POST_COMMENT } from "../constant/userConstant";

const initialState = {
  postComments: [],
};

const userCommentReducer = (state = initialState, action) => {
  switch (action.type) {
    case POST_COMMENT:
      state.postComments.push(action.payload);

      return { ...state };

    default:
      return state;
  }
};

export default userCommentReducer;
