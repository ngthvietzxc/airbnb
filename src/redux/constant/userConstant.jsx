export const ADMIN_LOGIN = "ADMIN_LOGIN";
export const USER_LOGIN = "USER_LOGIN";
export const SET_USER_LOGIN = "SET_USER_LOGIN";
export const USER_SIGNUP = "USER_SIGNUP";
export const SET_USER_SIGNUP = "SET_USER_SIGNUP";
export const SET_ORDER_ROOM = "SET_ORDER_ROOM";
export const POST_COMMENT = "POST_COMMENT";
