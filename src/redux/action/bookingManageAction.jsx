import { message } from "antd";
import { bookingManageService } from "../../Services/bookingManageService";
import {
  DELETE_BOOKING,
  GET_BOOKING,
  GET_BOOKING_BY_ID,
} from "../constant/bookingManageConstant";

export const getBooking = () => (dispatch) => {
  bookingManageService
    .getBooking()
    .then((res) => {
      dispatch({
        type: GET_BOOKING,
        payload: res.data.content,
      });
    })
    .catch((err) => {
      console.log(err);
    });
};
export const deleteBooking = (id) => (dispatch) => {
  bookingManageService
    .deleteBooking(id)
    .then(() => {
      dispatch({
        type: DELETE_BOOKING,
        payload: id,
      });
      message.success("Xóa đặt phòng thành công");
    })
    .catch((err) => {
      console.log(err);
      message.error("Xóa đặt phòng thất bại.");
    });
};
export const setAddBooking = (bookingData) => {
  return (dispatch) => {
    bookingManageService
      .addBooking(bookingData)
      .then(() => {
        bookingManageService.getBooking().then((res) => {
          dispatch({
            type: GET_BOOKING,
            payload: res.data.content,
          });
        });
        message.success("Thêm đặt phòng thành công");
      })
      .catch((err) => {
        console.log(err);
        message.error("Thêm đặt phòng thất bại.");
      });
  };
};
export const getBookingById = (id) => (dispatch) => {
  bookingManageService
    .getBookingById(id)
    .then((res) => {
      dispatch({
        type: GET_BOOKING_BY_ID,
        payload: res.data.content,
      });
    })
    .catch((err) => {
      console.log(err);
    });
};
export const putBookingById = (id, bookingDataById) => (dispatch) => {
  bookingManageService
    .putBookingById(id, bookingDataById)
    .then(() => {
      bookingManageService.getBookingById(id).then((res) => {
        dispatch({
          type: GET_BOOKING_BY_ID,
          payload: res.data.content,
        });
      });
      message.success("Chỉnh sửa thông tin thành công");
    })
    .catch((err) => {
      console.log(err);
      message.error("Chỉnh sửa thông tin thất bại");
    });
};
