import { postOderRoom } from "../../Services/orderServ";
import { postSignUp } from "../../Services/userServ";

import {
  SET_ORDER_ROOM,
  SET_USER_SIGNUP,
  USER_SIGNUP,
} from "../constant/userConstant";

// redux-thunk: gọi api trực tiếp trong action

export const setUserOrderRoomAction = (orderRoomForm, onSuccess, onError) => {
  return (dispatch) => {
    postOderRoom(orderRoomForm)
      .then((res) => {
        console.log(res);

        dispatch({
          type: SET_ORDER_ROOM,
          orderRoom: res.data.content,
        });
        onSuccess();
      })
      .catch((err) => {
        console.log(err);
        if (
          err.response &&
          err.response.data &&
          err.response.data.content === "Email đã tồn tại !"
        ) {
          onError("Yêu cầu không hợp lệ, email đã tồn tại");
        }
      });
  };
};
export const setUserOrderRoom = (userData) => {
  return { type: SET_ORDER_ROOM, payload: userData };
};
