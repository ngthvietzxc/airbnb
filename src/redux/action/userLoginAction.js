import { localUserServ } from "../../Services/localServ";
import { postSignIn } from "../../Services/userServ";
import { userService } from "../../Services/userService";
import { USER_LOGIN, SET_USER_LOGIN } from "../constant/userConstant";

// redux-thunk: gọi api trực tiếp trong action

export const setUserLoginAction = (formData, onSuccess, onError) => {
  return (dispatch) => {
    postSignIn(formData)
      .then((res) => {
        console.log(res);

        dispatch({
          type: USER_LOGIN,
          userInfor: res.data.content,
        });
        localUserServ.set(res.data.content);
        onSuccess();
      })
      .catch((err) => {
        // onError();
        console.log(err);
        if (
          err.response &&
          err.response.data &&
          err.response.data.content === "Email hoặc mật khẩu không đúng !"
        ) {
          onError("Sai email hoặc mật khẩu");
        } else if (
          err.response &&
          err.response.data &&
          err.response.data.content === "INVALID_PASSWORD"
        ) {
          onError("Sai mật khẩu");
        } else {
          onError("Lỗi đăng nhập");
        }
      });
  };
};
export const setUserLogin = (userData) => {
  return { type: SET_USER_LOGIN, payload: userData };
};
