import { userService } from "../../Services/userService";

export const ADMIN_LOGIN = "ADMIN_LOGIN";

// redux-thunk: gọi api trực tiếp trong action

export const setUserLoginAction = (formData, onSuccess, onError) => {
  return (dispatch) => {
    userService
      .postLogin(formData)
      .then((res) => {
        console.log("Đây là token sau khi đnăg nhập: ", res.data.content.token);
        console.log(res);
        dispatch({ type: ADMIN_LOGIN, payload: res.data });
        onSuccess(res);
      })
      .catch((err) => {
        console.log(err);
        if (
          err.response &&
          err.response.data &&
          err.response.data.content === "INVALID_EMAIL"
        ) {
          onError("Sai email hoặc mật khẩu");
        } else if (
          err.response &&
          err.response.data &&
          err.response.data.content === "INVALID_PASSWORD"
        ) {
          onError("Sai mật khẩu");
        } else {
          onError("Lỗi đăng nhập");
        }
      });
  };
};
export const setUserLogin = (userData) => {
  return { type: "SET_USER_LOGIN", payload: userData };
};
