import { postOderRoom } from "../../services/orderServ";
import { postComments, postSignUp } from "../../services/userServ";

import { POST_COMMENT } from "../constant/userConstant";

// redux-thunk: gọi api trực tiếp trong action

export const setUserCommentAction = (
  formComment,
  userToken,
  onSuccess,
  onError
) => {
  return (dispatch) => {
    postComments(formComment, userToken)
      .then((res) => {
        console.log(res);

        dispatch({
          type: POST_COMMENT,
          postComments: res.data.content,
        });
        onSuccess();
      })
      .catch((err) => {
        console.log(err);
      });
  };
};
export const setUserComment = (userData) => {
  return { type: POST_COMMENT, payload: userData };
};
